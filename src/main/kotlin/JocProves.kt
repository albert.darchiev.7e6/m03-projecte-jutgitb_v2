import kotlinx.serialization.Serializable

@Serializable
data class JocProves(val entrada: String, val sortida: String){
}