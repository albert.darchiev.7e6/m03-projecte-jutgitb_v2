import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

@Serializable
data class Problema(val enunciat: String, val jocProvesPub: JocProves, val jocProvesPriv: JocProves, var resolt: Boolean, var intents: MutableList<String>){
    fun solveProblem(answer: String): Boolean {
        if (jocProvesPriv.sortida == answer){
            resolt = true
            jsonFile.writeText(Json.encodeToString(problemsList))
            return true
        }
        else {
            intents.add(answer)
            jsonFile.writeText(Json.encodeToString(problemsList))
            return false
        }
    }
    fun getProblemInfo(): List<String> {
        return listOf(enunciat,"\nENTRADA: ${jocProvesPub.entrada}", "\nSORTIDA: ${jocProvesPub.sortida}", "\n\nENTRADA: ${jocProvesPriv.entrada}", "\nSORTIDA: " )
    }
    fun getGrade():Double{
        if (!resolt) return 0.0
        else {
            return 1.0 - (0.1 * intents.size)
        }
    }

    fun resoltStr():String{
        if(resolt) return "SI"
        else return "NO"
    }
}