import java.util.*
import java.io.File
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.sql.DriverManager

//Albert Darchiev
//M03 UF4 Projecte troncal v2

// CONTRASEÑA MODE PROFESSOR -> itb2023

//ESTABLECER CONEXION BASE DE DATOS
val jdbcUrl = "jdbc:postgresql://localhost:5432/jutge"
val connection = DriverManager
    .getConnection(jdbcUrl, "sjo", "")
//---------------------------------

val scanner = Scanner(System.`in`)
val jsonFile = File("src/main/kotlin/problemes.json")
var professorMode = false
var problemsList = mutableListOf<Problema>()

fun main() {
    updateProblemsList()

    updateDB()
    checkIfProfessor()
    menu()
}

fun updateDB(){
    // List TO DB
    val st = connection.createStatement()
//    st.execute("INSERT INTO problema (enunciat, jocProves_public_id, jocProves_privat_id, resolt, intents) VALUES (\'Enunciat Prova1\', jocProves{('entradaPUB', 'sortidaPUB'), ('entradaPV', 'sortidaPV')}, $intentsArray);")
    for (problema in problemsList){
//        problema.intents.add("1211")
        println(problema.jocProvesPub.entrada)
        println(problema.jocProvesPub.sortida)
        val intentsArray = if (problema.intents.size == 0) "ARRAY[]::text[]"
                            else ("ARRAY "+ problema.intents )
        println(problema.intents)

        st.execute("INSERT INTO problema (enunciat, resolt, intents) VALUES (\'${problema.enunciat}\', ${problema.resolt}, $intentsArray);")
        // INSERT INTO problema (enunciat, resolt) VALUES ( 'L''usuari escriu', false);
//        st.execute("INSERT INTO jocproves (entrada, sortida) VALUES (\'${problema.jocProvesPub.entrada}\', \'${problema.jocProvesPub.sortida}\');")
//        st.execute("INSERT INTO jocproves (entrada, sortida) VALUES (\'${problema.jocProvesPriv.entrada}\', \'${problema.jocProvesPriv.sortida}\');")

    }
}

fun menu(){
    updateProblemsList()
    if (professorMode){
        clearTerminal()
        println("-------- MODE PROFESSOR --------")
        println("1. Afegir problemes \n2. Treure report feina alumne \n3. Sortir \n4. Cambiar mode")
        when (scanner.nextInt()){
            1 -> addNewProblem()
            2 -> getGrades()
            3 -> println("FINS AVIAT!")
            4 -> main()
            else -> println("Opció Invalida")
        }
    }
    else {
        clearTerminal()
        println("-------- MODE ALUMNE --------")
        println("1. Seguir amb l’itinerari d’aprenentatge \n2. Llista de problemes \n3. Històric de problemes resolts \n4. Ajuda \n5. Sortir \n6. Cambiar mode")
        when (scanner.nextInt()) {
            1 -> continueWithProblems()
            2 -> allProblems()
            3 -> showProblemsHistoric()
            4 -> showHelp()
            5 -> println("FINS AVIAT!")
            6 -> main()
            else -> println("Opció Invalida")
        }
    }
}
fun checkIfProfessor(){
    clearTerminal()
    val password = "itb2023"
    println("SELECCIONA EL MODE:" +
            "\n1 -> MODE ALUMNE " +
            "\n2 -> MODE PROFESSOR")
    val option = scanner.nextInt()
    if (option == 1) professorMode = false
    else if(option == 2) {
        var repeat = false
        do{
            println("Introdueix contrasenya:")
            if (scanner.next() == password) {
                professorMode = true
                menu()
            }
            else {
                println("INCORRECTE. \nTornar a intentar?  SI | NO")
                repeat = (scanner.next().uppercase() == "SI")
            }
        }while (repeat)
        checkIfProfessor()
    }
}

fun continueWithProblems(){ // STUDENT MENU OPTION 1
    clearTerminal()
    problemsList.forEachIndexed { index, problema ->
        if (!problema.resolt) {
            val enunciats = problema.getProblemInfo()
            //SHOW PUBLIC EXAMPLE
            clearTerminal()
            println("\nvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv PROBLEMA ${index+1} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n")
            for (i in 0 until 3){
                print(enunciats[i])
            }
            println("\n\nVols resoldre el problema?  SI | NO ")
            if (scanner.next().uppercase() == "SI") {
                do {
                    //---------------- PUBLIC + PRIVATE GAME ----------------
                    var repeat = false
                    println("\nvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv PROBLEMA ${index+1} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n")
                    for (enunciat in enunciats){
                        print(enunciat)
                    }
                    // ------------------------------------------------------
                    val answer = scanner.next()
                    val resolt = problema.solveProblem(answer)
                    if (!resolt){
                        println("INCORRECTE. Vols tornar a intentar-ho?  SI | NO")
                        repeat = scanner.next().uppercase() == "SI"
                        clearTerminal()
                    }
                    else println("CORRECTE!")
                }while(repeat)
            }
            jsonFile.writeText(Json.encodeToString(problemsList)) //Guardar en JSON
        }
        if (index == problemsList.size-1) menu() //Vuelve al menu cuando llega al ultimo problema
    }
}

fun allProblems(){ // STUDENT MENU OPTION 2
    clearTerminal()
    problemsList.forEachIndexed { index, problema ->
        val enunciats = problema.getProblemInfo()
        println("\n\nvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv PROBLEMA ${index + 1} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n")
        for (enunciat in 0 until 3){
            print(enunciats[enunciat])
        }
    }
    println("\n\nQuin problema vols resoldre? \nIntrodueix el num. de l'exercici:")
    val problemNum = scanner.nextInt()-1
    clearTerminal()
    do {
        //---------------- PUBLIC + PRIVATE GAME ----------------
        var repeat = false
        println("\n\nvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv PROBLEMA ${problemNum + 1} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n")
        problemsList[problemNum].getProblemInfo().forEach{ print(it) }
        // ------------------------------------------------------
        val answer = scanner.next()
        val resolt = problemsList[problemNum].solveProblem(answer)
        if (!resolt){
            println("INCORRECTE. Vols tornar a intentar-ho?  SI | NO")
            repeat = scanner.next().uppercase() == "SI"
            clearTerminal()
        }
        else println("\nCORRECTE!")
    }while(repeat)
    println("Vols resoldre un altre?  SI | NO")
    val doAnother = scanner.next().uppercase() == "SI"
    if (doAnother) allProblems()
    else menu()
}

fun showProblemsHistoric(){ // STUDENT MENU OPTION 3
    clearTerminal()
    val read = jsonFile.readText()
    val problemsJson = Json.decodeFromString<List<Problema>>(read)
    println("EXERCICIS  --  NºINTENTS  --  RESOLT -- INTENTS")
    println("------------------------------------------------")
    problemsJson.forEachIndexed { index, problema ->
        if (problema.resolt) println("Exercici${index+1}  -       ${problema.intents.size}      -     SI   -> ${problema.intents}")
        else println("Exercici${index+1}  -       ${problema.intents.size}      -     NO   -> ${problema.intents}")
    }
    println("Tornar al menu?  SI | NO")
    if (scanner.next().uppercase() == "SI") menu()
    else println("ADEU!")
}

fun showHelp(){ // STUDENT MENU OPTION 4
    clearTerminal()
    println("---------------- FUNCIONAMENT JUTGv2 --------------------")
    println("En executar el programa apareixerà un menú amb 5 opcions:\n" +
            "\n---- Seguir amb l'itinerari d'aprenentatge ---- \n--> Aquesta opció mostra els problemes que encara no han estat resolts i permet a l'usuari intentar resoldre'ls.\n" +
            "\n---- Llista de problemes ---- \n--> Aquesta opció mostra tots els problemes disponibles i permet a l'usuari seleccionar-ne un per resoldre'l.\n" +
            "\n---- Històric de problemes resolts ---- \n--> Aquesta opció mostra un històric dels problemes resolts i el nombre d'intents realitzats per a cada un.\n" +
            "\n---- Ajuda ---- \n--> Aquesta opció mostra informació sobre el funcionament del programa.\n" +
            "\n---- Sortir ---- \n--> Aquesta opció tanca el programa.")

    println("Tornar al menu?  SI | NO")
    if (scanner.next().uppercase() == "SI") menu()
    else println("ADEU!")
}

fun addNewProblem(){
    clearTerminal()
    println("----- AFEGIR PROBLEMA NOU -----")
    println("\n --------- JOC PUBLIC ---------")
    println("Enunciat: ")
    scanner.nextLine()
    val enunciat = scanner.nextLine().toString()
    println("Public Input: ")
    val input = scanner.nextLine().toString()
    println("Public Output:")
    val output = scanner.nextLine().toString()

    println("\n --------- JOC PRIVAT ---------")
    println("Private Input:")
    val privInput = scanner.nextLine().toString()
    println("Private Output:")
    val privOutput = scanner.nextLine().toString()

    val newProblem = Problema(enunciat, JocProves(input, output), JocProves(privInput, privOutput), false, mutableListOf())

    clearTerminal()
    newProblem.getProblemInfo().forEach{print(it)}
    println("Vols guardar el problema?  SI | NO")

    if (scanner.next() == "SI"){
        problemsList.add(newProblem)
        jsonFile.writeText(Json.encodeToString(problemsList))
    }
    else menu()
}

fun getGrades(){
    val gradesList = mutableListOf<Double>()
    println("Tenir en compte problemes NO resolts?  SI | NO")
    val justSolved = scanner.next().uppercase() == "SI"
    clearTerminal()
    println("PROBLEMA  - INTENTS  -  RESOLT  -  PUNTUACIÓ")
    problemsList.forEachIndexed { idx, problema ->
        println("Problema${idx+1}  -   ${problema.intents.size}     -   ${problema.resoltStr()}    -   ${problema.getGrade()}")
        if (justSolved || problema.resolt) gradesList.add(problema.getGrade())
    }
    println("\nNOTA FINAL:  ${(gradesList.sum() / gradesList.size) * 10} / 10")

    println("Tornar al menu?  SI | NO")
    if (scanner.next().uppercase() == "SI") menu()
    else println("ADEU!")
}
fun updateProblemsList(){
    val jsonString = jsonFile.readText()
    problemsList = Json.decodeFromString(jsonString)
}
fun clearTerminal(){
    repeat(30){
        println()
    }
}